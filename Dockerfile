# vim:set ft=dockerfile:

FROM python:2.7

MAINTAINER Paolo Casciello <paolo.casciello@scalebox.it>

RUN apt-get update && apt-get install -y --no-install-recommends \
	build-essential \
	python-dev \
	uwsgi-plugin-python uwsgi-plugin-http


